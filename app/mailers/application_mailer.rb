class ApplicationMailer < ActionMailer::Base
  #default from = 送信元のメールアドレス
  default from: 'noreply@example.com'
  layout 'mailer'
end
