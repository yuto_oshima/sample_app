class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update] #(1)の対応 = パスワード再設定の有効期限が切れていないか
  def new
  end

  def create
  	@user = User.find_by(email: params[:password_reset][:email].downcase)
  	if @user
  		@user.create_reset_digest
  		@user.send_password_reset_email
  		flash[:info] = "Email sent with password reset instructions"
  		redirect_to root_url
  	else
  		flash.now[:danger] = "Email address not found" #flash.nowでその画面までの表示指定
  		render 'new'
  	end
  end

  def edit
  end

  def update
    if params[:user][:password].empty? #(3)の対応 = 新しいパスワードが空文字列になっていないか (ユーザー情報の編集ではOKだった), allow_nilでnilを許可したため
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update_attributes(user_params) #(4)の対応　= 新しいパスワードが正しければ、更新する
      log_in @user
      @user.update_attribute(:reset_digest, nil) #パスワードリセットが完了したら、同じURLから再設定出来ない
      flash[:success] = "password has been reset"
      redirect_to @user
    else
      render 'edit' #(2)の対応 = 無効なパスワードであれば失敗させる (失敗した理由も表示する)
    end
  end


  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation) 
      #戻り値(password: "hogehoge", password_confirmaton: "hogehoge")
      #params.require(:user) = params[:user]を表す
      #.permit(:password, :password_confirmation) = params[:user][:password] と params[:user][:password_confirmation]のみを許可する
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

    #正しいユーザーかどうか確認する
    def valid_user
      unless (@user && @user.activated? && @user.authenticated?(:reset, params[:id]))
        redirect_to root_url
      end
    end

    def check_expiration
      if @user.password_reset_expired?
        flash[:danger] = "Password reset has expired."
        redirect_to new_password_reset_url
      end
    end
end
