require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

	def setup
		@user = users(:michael)
	end

	test "layout links" do
		#ログイン前のテスト
		get root_path
		assert_template 'static_pages/home'
		assert_select "a[href=?]", root_path, count: 2
		assert_select "a[href=?]", help_path
		assert_select "a[href=?]", about_path
		assert_select "a[href=?]", contact_path
		assert_select "a[href=?]", signup_path
		get contact_path
		assert_select "title", full_title("Contact")
		get signup_path
		assert_select "title", full_title("Sign up")
		#ログイン後のテスト
		log_in_as(@user)
		get users_path
		assert_template 'users/index'
		assert_select "a[href=?]", root_path, count: 2
		assert_select "a[href=?]", help_path
		assert_select "a[href=?]", about_path
		assert_select "a[href=?]", contact_path 
		assert_select "a[href=?]", users_path
		assert_select "a[href=?]", user_path(@user)
		assert_select "a[href=?]", edit_user_path(@user)
		assert_select "a[href=?]", logout_path
		#ログイン後のroot_path
		get root_path
		assert_template 'static_pages/home'
		assert_select "div.stats>a[href=?]>strong#following", following_user_path(@user)#タグがあるか確認する
		assert_select 'div.stats>a[href=?]>strong#followers', followers_user_path(@user)#タグがあるか確認する
		assert_match "following", response.body
		assert_match "followers", response.body
		assert_match @user.following.count.to_s, response.body
		assert_match @user.followers.count.to_s, response.body
	end
	
end
