require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'users/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
  	name = "Foo Bar"
  	email = "foo@bar.com"
  	patch user_path(@user),params: {user: {name: name, email: email, password: "", password_confirmation: ""}}
  	assert_not flash.empty? #更新成功時にflashを表示させる
  	assert_redirected_to @user
  	@user.reload #データベースが更新されても変数は変わらないので"reload"して更新する
  	assert_equal name, @user.name
  	assert_equal email, @user.email
  end

  #リスト10.2(演習1)
  test "forwarding is true" do 
    get edit_user_path(@user)
    assert_equal session[:forwarding_url], edit_user_url(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    assert session[:forwarding_url].nil?
  end

end