require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    ActionMailer::Base.deliveries.clear #"配列deliveriesは変数なので、setupメソッドでこれを初期化しておかないと、並行して行われる他のテストでメールが配信されたときにエラーが発生してしまいます"
  end

  test "invalid singnup information" do
  	get signup_path
  	assert_select "form[action=?]", signup_path
  	assert_no_difference 'User.count' do
  		post signup_path,params: {user: {name: "", email: "user@invalid", password: "foo", password_confirmation: "bar"} }
  	end
  	assert_template 'users/new'
  	assert_select 'div#error_explanation', true
 	assert_select 'div#error_explanation ul li', 4 
  end

  test "valid signup information with account activation" do
  	get signup_path
  	assert_select "form[action=?]", signup_path
  	assert_difference 'User.count', 1 do
  		post users_path,params: { user: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
  	end
    assert_equal 1, ActionMailer::Base.deliveries.size #sizeでメールの個数を観れる
    user = assigns(:user) #assigns = users_controller内の@userを参照している
    assert_not user.activated?  
    #有効化していない状態でログインしてみる
    log_in_as(user)
    assert_not is_logged_in?
    #有効化トークンが不正な場合
    get edit_account_activation_path("Invaild token", email: user.email)
    assert_not is_logged_in?
    #トークンは正しいがメールアドレスが不正な場合
    get edit_account_activation_path(user.activation_token, email: "wrong")
    assert_not is_logged_in?
    #有効化トークンが正しい場合
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
  	follow_redirect!
  	assert_template 'users/show'
  	assert flash.present?
    assert is_logged_in?
  end
end
